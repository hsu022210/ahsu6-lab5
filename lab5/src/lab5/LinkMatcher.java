package lab5;


import java.net.URL;
import java.util.*;
import java.util.regex.*;

import java.io.IOException;

import javax.net.ssl.*;
import java.io.*;


/** A class for finding links in an html file.
 *  Modified by Prof. Karpenko from the assignment of Prof. Engle. 
 */
public class LinkMatcher {

	// This regex should match an HTML anchor tag such as <a  href  = "http://cs.www.usfca.edu"  >"
	// where the actual hyperlink is captured in a group.
	// See the following link regarding the format of the anchor tag: https://developer.mozilla.org/en-US/docs/Web/HTML/Element/a
	public static final String REGEX = "(?i)<a[A-Za-z\"=_ ]+href\\s*=\\s*\"((http|/\\w+|https)(.*?))\".*?>.*?</a>"; // FILL IN!
	
	/**
	 * Take an html file and return a list of hyperlinks in that html 
	 * that satisfy the following requirements:
	 * 1. The list should not contain duplicates. 
	 * For the purpose of this assignment, duplicates are the links that are the same, except for the fragment. 
	 * Example: you should consider these two links as equal"
	 * "java/lang/StringBuffer.html#StringBuffer" and "java/lang/StringBuffer.html#StringBuffer-java.lang.String"
	 * (because they are the same if you remove the fragment).
	 * 
	 * 2. Do not include links that take you to the same page (links that start with the fragment).

	 * You are required to use a regular expression to find links 
	 * (use variable REGEX defined on top of the class - fill in the actual pattern).
	 * You are required to use classes Pattern and Matcher in this method. 
	 * Do not use any other classes or packages (except String, ArrayList, Pattern,  Matcher, BufferedReader etc.)
	 * 
	 * @param filename
	 *            The name of the HTML file.
	 * @return An ArrayList of links
	 */
	public static List<String> findLinks(String filename) {
		List<String> links = new ArrayList<>();

		try{
			BufferedReader in = new BufferedReader(new FileReader(filename));
			String line;
			StringBuffer sb = new StringBuffer();

			while ((line = in.readLine()) != null) {
//				System.out.println(line);
				sb.append(line);
			}

			String s = sb.toString();
			//crawl here

			Pattern p = Pattern.compile(REGEX);
			Matcher m = p.matcher(s);

			while (m.find()) {
				String theUrl = "";
				if (m.group(1).contains("#")){
					String[] parts = m.group(1).split("#");
//					System.out.println(parts[0]);
					theUrl = parts[0];
				}else {
//					System.out.println("group1" + m.group(1));
					theUrl = m.group(1);
				}

				if (!links.contains(theUrl)){
					links.add(theUrl);
				}
			}

		}catch (FileNotFoundException e){
			e.printStackTrace();
		}catch (IOException e){
			e.printStackTrace();
		}

		// FILL IN CODE
		return links;

	}
	
	
	/**
	 * Take a URL, fetch an html page at this URL (using sockets),  and find all unique hyperlinks on that webpage. 
	 * The list should not contain "duplicates" (see the previous comment)
	 * or links that take you to the same page.
	 * The difference with the previous method is that it should fetch the HTML from the server first.
	 * @param url
	 * @return An ArrayList of links
	 */
	public static List<String> fetchAndFindLinks(String urlString) {
		List<String> links = new ArrayList<>();

		PrintWriter out = null;
		BufferedReader in = null;
		SSLSocket socket = null;
		try {
			URL url = new URL(urlString);

			SSLSocketFactory factory = (SSLSocketFactory) SSLSocketFactory.getDefault();

			// HTTPS uses port 443
			socket = (SSLSocket) factory.createSocket(url.getHost(), 443);

			// output stream for the secure socket
			out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
			String request = getRequest(url.getHost(), url.getPath() + "?" + url.getQuery());
			System.out.println("Request: " + request);

			out.println(request); // send a request to the server
			out.flush();

			// input stream for the secure socket.
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

			// use input stream to read server's response
			String line;
			StringBuffer sb = new StringBuffer();

			String status = "header";
			while ((line = in.readLine()) != null) {
				if(status == "body"){
//                        System.out.println(line);
					sb.append(line);
				}

				if (line.contains("Connection: close")){
					status = "body";
				}
			}

			String s = sb.toString();
			//crawl here

			Pattern p = Pattern.compile(REGEX);
			Matcher m = p.matcher(s);

			while (m.find()) {
				String theUrl = "";
				if (m.group(1).contains("#")){
					String[] parts = m.group(1).split("#");
//					System.out.println(parts[0]);
					theUrl = parts[0];
				}else {
//					System.out.println("group1" + m.group(1));
					theUrl = m.group(1);
				}

				if (!links.contains(theUrl)){
					links.add(theUrl);
				}
			}

		} catch (IOException e) {
			System.out.println(
					"An IOException occured while writing to the socket stream or reading from the stream: " + e);
		} finally {
			try {
				// close the streams and the socket
				out.close();
				in.close();
				socket.close();
			} catch (IOException e) {
				System.out.println("An exception occured while trying to close the streams or the socket: " + e);
			}
		}
		
		
		// FILL IN CODE
		return links;

	}

	private static String getRequest(String host, String pathResourceQuery) {
		String request = "GET " + pathResourceQuery + " HTTP/1.1" + System.lineSeparator() // GET
				// request
				+ "Host: " + host + System.lineSeparator() // Host header required for HTTP/1.1
				+ "Connection: close" + System.lineSeparator() // make sure the server closes the
				// connection after we fetch one page
				+ System.lineSeparator();
		return request;
	}

	

}
